const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const fs = require('fs');
const path = require('path');
const resolvers = require('./resolvers');

const typeDefs = fs.readFileSync(path.resolve(__dirname, './schema.gql'), {
  encoding: 'utf8',
});

const server = new ApolloServer({ typeDefs, resolvers });

const app = express();

server.applyMiddleware({ app, cors: 'localhost:8080' });

app.listen({ port: 4000 }, () =>
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`),
);
