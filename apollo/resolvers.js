const shortid = require('shortid');
const lowDb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('heroes.json');
const db = lowDb(adapter);

module.exports = {
  Query: {
    allHeroes: () => db.get('heroes').value(),
    getHero: (root, { name }) =>
      db
        .get('heroes')
        .find({ name })
        .value(),
  },

  Mutation: {
    addHero: (root, { hero }, { pubsub }) => {
      const newHero = {
        id: shortid.generate(),
        name: hero.name,
        image: hero.image || '',
        twitter: hero.twitter || '',
        github: hero.github || '',
      };
      db.get('heroes')
        .push(newHero)
        .last()
        .write();

      pubsub.publish('heroes', { addHero: newHero });

      return newHero;
    },
    deleteHero: (root, { name }) => {
      db.get('heroes')
        .remove({ name })
        .write();

      return true;
    },
  },
};
