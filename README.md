# gitlab-vuex-apollo

This project contains 3 branches for different implementations of state management with GraphQL and Apollo.

- [Vuex state with Apollo queries as promises](https://gitlab.com/ntepluhina/gitlab-vuex-apollo/tree/vuex-promises)
- [Vuex state with Apollo queries as subscriptions](https://gitlab.com/ntepluhina/gitlab-vuex-apollo/tree/vuex-subscriptions)
- [apollo-link-state](https://gitlab.com/ntepluhina/gitlab-vuex-apollo/tree/apollo-link-state)

To start the GraphQL server, please run

```bash
yarn apollo
```

To run the project, please select the branch, install node modules and run

```bash
yarn serve
```
