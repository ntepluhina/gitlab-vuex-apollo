import Vue from 'vue';
import Vuex from 'vuex';
import ApolloClient from 'apollo-boost';
import allHeroesQuery from './queries/allHeroes.gql';

Vue.use(Vuex);

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
});

client.defaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
  },
};

export default new Vuex.Store({
  state: {
    heroes: [],
    loading: false,
  },
  mutations: {
    setHeroes(state, heroes) {
      state.heroes = heroes;
    },
    setLoadingStatus(state, loading) {
      state.loading = loading;
    },
  },
  actions: {
    setHeroes({ commit }) {
      const response = client.watchQuery({
        query: allHeroesQuery,
      });

      response.subscribe(res => {
        if (res.loading) {
          commit('setLoadingStatus', true);
        } else {
          commit('setLoadingStatus', false);
          commit('setHeroes', res.data.allHeroes);
        }
      });
    },
  },
});
